doc: ragged2e.pdf

ragged2e.pdf:	ragged2e.dtx ragged2e.sty
	pdflatex -draftmode ragged2e.dtx
	pdflatex -draftmode ragged2e.dtx
	makeindex -s gind.ist -t ragged2e.ind.ilg ragged2e ragged2e.idx
	makeindex -s gglo.ist -t ragged2e.gls.ilg -o ragged2e.gls ragged2e ragged2e.glo
	pdflatex ragged2e.dtx

ragged2e.sty:	ragged2e.ins
	latex ragged2e.ins

ragged2e.zip:	ragged2e.dtx ragged2e.pdf ragged2e.ins
	mkdir -p ragged2e
	@cp -a ragged2e.dtx ragged2e.pdf ragged2e.ins README.md ragged2e/.
	@zip -ll -r ragged2e.zip ragged2e
	@rm -rf ragged2e

.PHONY:	clean clobber

clean: 
	rm -f *.aux *.toc *.glo *.gls *.hd *.idx *.ilg *.ind *.log

clobber:	clean
	rm -f *.pdf *.sty *.zip

