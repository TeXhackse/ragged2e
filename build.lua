#!/usr/bin/env texlua

module = "ragged2e"
typesetsuppfiles={"gitHeadInfo.gin"}
checkengines = {"pdftex"}

tagfiles = {"*.dtx", "*.md"}

function update_tag(file,content,tagname,tagdate)
	local replaced = string.gsub(content,"%d%d%d%d/%d%d/%d%d v%d+.%d", tagdate.." v"..tagname)
	replaced = string.gsub(replaced,"\\changes{v"..tagname.."}{%d%d%d%d/%d%d/%d%d", "\\changes{v"..tagname.."}{"..tagdate)
	return replaced
end
